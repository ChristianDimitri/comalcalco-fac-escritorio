﻿Public Class FrmSupervisorNew
    Private Sub Acceso(ByVal usuario As String, ByVal password As String, ByVal op As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, usuario, 10)
        BaseII.CreateMyParameter("@Pas", SqlDbType.VarChar, password, 10)
        BaseII.CreateMyParameter("@op", SqlDbType.SmallInt, op)
        BaseII.CreateMyParameter("@bnd", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("VerAccesoSupervisor2")
        flagSupervisor2 = BaseII.dicoPar("@bnd").ToString()
        If flagSupervisor2 Then
            LocSupBon = usuario
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        ElseIf op = 0 Then
            MsgBox("Usuario incorrecto", MsgBoxStyle.Exclamation)
        End If
    End Sub
    Private Sub OK_Click(sender As Object, e As EventArgs) Handles OK.Click
        Acceso(Clv_UsuarioTextBox.Text, PasaporteTextBox.Text, 0)
    End Sub

    Private Sub FrmSupervisorNew_Load(sender As Object, e As EventArgs) Handles Me.Load
        flagSupervisor2 = False
        Acceso(GloUsuario, "", 1)
    End Sub

    Private Sub Cancel_Click(sender As Object, e As EventArgs) Handles Cancel.Click
        flagSupervisor2 = False
        Me.DialogResult = Windows.Forms.DialogResult.No
        Me.Close()
    End Sub
End Class