﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSupervisorNew
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CMBClv_UsuarioLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSupervisor))
        Me.Clv_UsuarioTextBox = New System.Windows.Forms.TextBox()
        Me.PasaporteTextBox = New System.Windows.Forms.TextBox()
        Me.NewsoftvDataSet = New softvFacturacion.NewsoftvDataSet()
        Me.VerAcceso1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VerAcceso1TableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.VerAcceso1TableAdapter()
        Me.Cancel = New System.Windows.Forms.Button()
        Me.OK = New System.Windows.Forms.Button()
        Me.LogoPictureBox = New System.Windows.Forms.PictureBox()
        Me.CMBPasswordLabel = New System.Windows.Forms.Label()
        Me.REDLabel1 = New System.Windows.Forms.Label()
        Me.ProcedimientosArnoldo3 = New softvFacturacion.ProcedimientosArnoldo3()
        Me.VerAcceso2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VerAcceso2TableAdapter = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        CMBClv_UsuarioLabel = New System.Windows.Forms.Label()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VerAcceso1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VerAcceso2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBClv_UsuarioLabel
        '
        CMBClv_UsuarioLabel.AutoSize = True
        CMBClv_UsuarioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBClv_UsuarioLabel.Location = New System.Drawing.Point(251, 30)
        CMBClv_UsuarioLabel.Name = "CMBClv_UsuarioLabel"
        CMBClv_UsuarioLabel.Size = New System.Drawing.Size(119, 15)
        CMBClv_UsuarioLabel.TabIndex = 2
        CMBClv_UsuarioLabel.Text = "Login Supervisor:"

        '
        'Clv_UsuarioTextBox
        '
        Me.Clv_UsuarioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_UsuarioTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_UsuarioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_UsuarioTextBox.Location = New System.Drawing.Point(254, 48)
        Me.Clv_UsuarioTextBox.MaxLength = 15
        Me.Clv_UsuarioTextBox.Name = "Clv_UsuarioTextBox"
        Me.Clv_UsuarioTextBox.Size = New System.Drawing.Size(116, 22)
        Me.Clv_UsuarioTextBox.TabIndex = 0
        '
        'PasaporteTextBox
        '
        Me.PasaporteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PasaporteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PasaporteTextBox.Location = New System.Drawing.Point(254, 114)
        Me.PasaporteTextBox.MaxLength = 10
        Me.PasaporteTextBox.Name = "PasaporteTextBox"
        Me.PasaporteTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.PasaporteTextBox.Size = New System.Drawing.Size(97, 22)
        Me.PasaporteTextBox.TabIndex = 1
        '
        'NewsoftvDataSet
        '
        Me.NewsoftvDataSet.DataSetName = "NewsoftvDataSet"
        Me.NewsoftvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'VerAcceso1BindingSource
        '
        Me.VerAcceso1BindingSource.DataMember = "VerAcceso1"
        Me.VerAcceso1BindingSource.DataSource = Me.NewsoftvDataSet
        '
        'VerAcceso1TableAdapter
        '
        Me.VerAcceso1TableAdapter.ClearBeforeFill = True
        '
        'Cancel
        '
        Me.Cancel.BackColor = System.Drawing.Color.Orange
        Me.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cancel.Location = New System.Drawing.Point(396, 169)
        Me.Cancel.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Size = New System.Drawing.Size(152, 35)
        Me.Cancel.TabIndex = 3
        Me.Cancel.Text = "&CANCELAR"
        Me.Cancel.UseVisualStyleBackColor = False
        '
        'OK
        '
        Me.OK.BackColor = System.Drawing.Color.Orange
        Me.OK.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OK.Location = New System.Drawing.Point(233, 169)
        Me.OK.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(155, 35)
        Me.OK.TabIndex = 2
        Me.OK.Text = "&ACEPTAR"
        Me.OK.UseVisualStyleBackColor = False
        '
        'LogoPictureBox
        '
        Me.LogoPictureBox.Image = CType(resources.GetObject("LogoPictureBox.Image"), System.Drawing.Image)
        Me.LogoPictureBox.Location = New System.Drawing.Point(5, 5)
        Me.LogoPictureBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.LogoPictureBox.Name = "LogoPictureBox"
        Me.LogoPictureBox.Size = New System.Drawing.Size(220, 221)
        Me.LogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.LogoPictureBox.TabIndex = 6
        Me.LogoPictureBox.TabStop = False
        '
        'CMBPasswordLabel
        '
        Me.CMBPasswordLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBPasswordLabel.Location = New System.Drawing.Point(251, 84)
        Me.CMBPasswordLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBPasswordLabel.Name = "CMBPasswordLabel"
        Me.CMBPasswordLabel.Size = New System.Drawing.Size(168, 27)
        Me.CMBPasswordLabel.TabIndex = 9
        Me.CMBPasswordLabel.Text = "&Contraseña Supervisor"
        Me.CMBPasswordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'REDLabel1
        '
        Me.REDLabel1.BackColor = System.Drawing.Color.Transparent
        Me.REDLabel1.CausesValidation = False
        Me.REDLabel1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.REDLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.REDLabel1.ForeColor = System.Drawing.Color.Red
        Me.REDLabel1.Location = New System.Drawing.Point(12, 10)
        Me.REDLabel1.Name = "REDLabel1"
        Me.REDLabel1.Size = New System.Drawing.Size(203, 35)
        Me.REDLabel1.TabIndex = 10
        Me.REDLabel1.Text = "Acceso Restringido"
        Me.REDLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ProcedimientosArnoldo3
        '
        Me.ProcedimientosArnoldo3.DataSetName = "ProcedimientosArnoldo3"
        Me.ProcedimientosArnoldo3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'VerAcceso2BindingSource
        '
        Me.VerAcceso2BindingSource.DataMember = "VerAcceso2"
        Me.VerAcceso2BindingSource.DataSource = Me.ProcedimientosArnoldo3
        '
        'VerAcceso2TableAdapter
        '
        Me.VerAcceso2TableAdapter.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'FrmSupervisor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(557, 231)
        Me.Controls.Add(Me.REDLabel1)
        Me.Controls.Add(Me.CMBPasswordLabel)
        Me.Controls.Add(Me.Cancel)
        Me.Controls.Add(Me.OK)
        Me.Controls.Add(Me.LogoPictureBox)
        Me.Controls.Add(Me.PasaporteTextBox)
        Me.Controls.Add(CMBClv_UsuarioLabel)
        Me.Controls.Add(Me.Clv_UsuarioTextBox)
        Me.Name = "FrmSupervisor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Acceso Restringido"
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VerAcceso1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VerAcceso2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NewsoftvDataSet As softvFacturacion.NewsoftvDataSet
    Friend WithEvents Clv_UsuarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PasaporteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents VerAcceso1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VerAcceso1TableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.VerAcceso1TableAdapter
    Friend WithEvents Cancel As System.Windows.Forms.Button
    Friend WithEvents OK As System.Windows.Forms.Button
    Friend WithEvents LogoPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents CMBPasswordLabel As System.Windows.Forms.Label
    Friend WithEvents REDLabel1 As System.Windows.Forms.Label
    Friend WithEvents ProcedimientosArnoldo3 As softvFacturacion.ProcedimientosArnoldo3
    Friend WithEvents VerAcceso2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VerAcceso2TableAdapter As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
End Class
