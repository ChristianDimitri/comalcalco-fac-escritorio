﻿Imports System.Data.SqlClient


Public Class CobroParcialMaterial
    Public Shared Function spInsertaParcialidadesCobroMaterial(ByVal prmContrato As Long, ByVal prmClvUnicaNet As Integer, ByVal prmImporte As Decimal, ByVal prmNumPagos As Int16, _
                                                               ByVal prmImporteXmes As Decimal) As Long
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("spInsertaParcialidadesCobroMaterial", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandTimeout = 0
        CMD.Parameters.AddWithValue("@CONTRATO", prmContrato)
        CMD.Parameters.AddWithValue("@Clv_UnicaNet", prmClvUnicaNet)
        CMD.Parameters.AddWithValue("@SUMA", prmImporte)
        CMD.Parameters.AddWithValue("@NumMaxPagos", prmNumPagos)
        CMD.Parameters.AddWithValue("@ImporteXmes", prmImporteXmes)

        Dim READER As SqlDataReader

        Try
            CON.Open()
            READER = CMD.ExecuteReader
            While (READER.Read)
                spInsertaParcialidadesCobroMaterial = READER(0).ToString
            End While
            MsgBox("Se ha estipulado exitosamente el Convenio #" + spInsertaParcialidadesCobroMaterial.ToString, MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Function

    Public Shared Function spConsultaParcialidadesCobroMaterial(ByVal prmIdConvenio As Integer) As DataTable
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("spConsultaParcialidadesCobroMaterial", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandTimeout = 0
        CMD.Parameters.AddWithValue("@idConvenio", prmIdConvenio)

        Dim DA As New SqlDataAdapter(CMD)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
            spConsultaParcialidadesCobroMaterial = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Function

    Public Shared Sub spEliminaParcialidadesCobroMaterial(ByVal prmIdConvenio As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("spConsultaParcialidadesCobroMaterial", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandTimeout = 0
        CMD.Parameters.AddWithValue("@idConvenio", prmIdConvenio)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Public Shared Function spCalculaConvenioCobroMaterial(ByVal prmClvUnicaNet As Integer) As DataTable
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("spCalculaConvenioCobroMaterial", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandTimeout = 0
        CMD.Parameters.AddWithValue("@Clv_UnicaNet", prmClvUnicaNet)
        CMD.Parameters.AddWithValue("@Clv_Session", gloClv_Session)


        Dim DA As New SqlDataAdapter(CMD)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
            spCalculaConvenioCobroMaterial = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Function

    Public Shared Function spDamePagosRestantes(ByVal prmClvDetFac As Integer) As Integer
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("spDamePagosRestantes", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandTimeout = 0
        CMD.Parameters.AddWithValue("@prmClvDetFac", prmClvDetFac)

        Dim READER As SqlDataReader

        Try
            CON.Open()
            READER = CMD.ExecuteReader
            While (READER.Read)
                spDamePagosRestantes = READER(0).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Function

    Public Shared Sub spAdelantaPagosParcialesMaterial(ByVal prmClvSession As Long, ByVal prmContrato As Long, ByVal prmNumMesesAdelantados As Integer, ByVal prmClvDetFac As Long, _
                                                        ByVal prmOp As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("spAdelantaPagosParcialesMaterial", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandTimeout = 0
        CMD.Parameters.AddWithValue("@clvSession", prmClvSession)
        CMD.Parameters.AddWithValue("@contrato", prmContrato)
        CMD.Parameters.AddWithValue("@NumMesesAdelantados", prmNumMesesAdelantados)
        CMD.Parameters.AddWithValue("@clvDetFac", prmClvDetFac)
        CMD.Parameters.AddWithValue("@op", prmOp)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Public Shared Function spReporteCobrosParcialesMaterial(ByVal prmContrato As Long, ByVal prmFechaIni As Date, ByVal prmFechaFin As Date, ByVal prmOp As Integer, _
                                                            ByVal prmSaldados As Boolean, ByVal prmPendientes As Boolean) As DataSet
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("spReporteCobrosParcialesMaterial", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandTimeout = 0
        CMD.Parameters.AddWithValue("@contrato", prmContrato)
        CMD.Parameters.AddWithValue("@fechaIni", prmFechaIni)
        CMD.Parameters.AddWithValue("@fechaFin", prmFechaFin)
        CMD.Parameters.AddWithValue("@saldado", prmSaldados)
        CMD.Parameters.AddWithValue("@pendientes", prmPendientes)
        CMD.Parameters.AddWithValue("@op", prmOp)

        Dim DA As New SqlDataAdapter(CMD)
        Dim DS As New DataSet()

        DA.Fill(DS)
        DS.Tables(0).TableName = "spReporteCobrosParcialesMaterial"

        spReporteCobrosParcialesMaterial = DS
    End Function
End Class
