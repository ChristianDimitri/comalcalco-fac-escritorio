Imports System.Net.NetworkInformation
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Sockets
Imports System.IO.StreamReader
Imports System.IO.File
Imports System.IO
Imports System
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Microsoft.VisualBasic


Public Class FrmRecepOxxoNew
    Dim ClvFacturaIni As Long = 0
    Dim LocNomarchivo As String = Nothing
    Dim ClvFacturaFin As Long = 0
    Dim Clv_Session_Oxxo As Long = 0
    Private customersByCityReport As ReportDocument
    Dim bndOxxo As Integer = 0

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub ConfigureCrystalReports(ByVal rangoini As Long, ByVal rangofin As Long)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        '        If GloImprimeTickets = False Then
        'reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        'End If

        customersByCityReport.Load(reportPath)
        'If GloImprimeTickets = False Then
        '    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        'End If
        SetDBLogonForReport(connectionInfo, customersByCityReport)


        '@Clv_Factura 
        customersByCityReport.SetParameterValue(0, "0")
        '@Clv_Factura_Ini
        customersByCityReport.SetParameterValue(1, CStr(rangoini))
        '@Clv_Factura_Fin
        customersByCityReport.SetParameterValue(2, CStr(rangofin))
        '@Fecha_Ini
        customersByCityReport.SetParameterValue(3, "01/01/1900")
        '@Fecha_Fin
        customersByCityReport.SetParameterValue(4, "01/01/1900")
        '@op
        customersByCityReport.SetParameterValue(5, 1)

        ' If GloImprimeTickets = True Then
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        ' End If

        customersByCityReport.PrintToPrinter(1, True, 0, 0)

        customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets

        bitsist(GloUsuario, 0, GloSistema, Me.Name, "Se Imprimieron las Facturas del Oxxo", "", "Nombre del Archivo:" + LocNomarchivo, SubCiudad)

        'CrystalReportViewer1.ReportSource = customersByCityReport

        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub



    Private Sub DameClv_Session_Oxxo(ByVal nombreArchivo As String)
        'Try
        '    Dim CON As New SqlConnection(MiConexion)
        '    Clv_Session_Oxxo = 0
        '    CON.Open()
        '    Dim LocClv_SessionBancos As Long = 0
        '    Dim comando As SqlClient.SqlCommand
        '    comando = New SqlClient.SqlCommand
        '    With comando
        '        .Connection = CON
        '        .CommandText = "DameClv_Session_Oxxo "
        '        .CommandType = CommandType.StoredProcedure
        '        .CommandTimeout = 0
        '        Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        '        prm.Direction = ParameterDirection.Output
        '        prm.Value = 0
        '        .Parameters.Add(prm)
        '        Dim prm1 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
        '        prm1.Direction = ParameterDirection.Input
        '        prm1.Value = GloUsuario
        '        .Parameters.Add(prm1)
        '        Dim i As Integer = comando.ExecuteNonQuery()
        '        Clv_Session_Oxxo = prm.Value
        '        CON.Close()
        '    End With
        'Catch ex As Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try
        Clv_Session_Oxxo = 0
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Usuario", System.Data.SqlDbType.VarChar, GloUsuario, 50)
        BaseII.CreateMyParameter("@nombreArchivo", System.Data.SqlDbType.VarChar, nombreArchivo, 8000)
        BaseII.CreateMyParameter("@Clv_Session", System.Data.ParameterDirection.Output, System.Data.SqlDbType.BigInt)
        Dim diccionario As New Dictionary(Of String, Object)
        diccionario = BaseII.ProcedimientoOutPut("DameClv_Session_Oxxo")
        Clv_Session_Oxxo = CLng(diccionario("@Clv_Session"))

    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Try

        '    Dim CON As New SqlConnection(MiConexion)
        '    'CON.Open()
        '    Dim prueba As String = Nothing
        '    Dim locerrorsantander As Integer = 0
        '    Dim proceso As Integer = 0
        '    Dim x As Integer = 0
        '    Dim y As Integer = 0
        '    Dim z As Integer = 0
        '    Dim clv_aceptado As String = Nothing
        '    Dim clv_id As String = Nothing
        '    Dim contrato As String = Nothing
        '    Dim Fila_1 As String = Nothing


        '    Me.OpenFileDialog1.FileName = ""
        '    Me.OpenFileDialog1.Filter = "Archivo Resultados *.txt|*.txt"
        '    Me.OpenFileDialog1.ShowDialog()


        '    If Me.OpenFileDialog1.FileName = "" Or Me.OpenFileDialog1.FileName = "OpenFileDialog1" Then
        '        MsgBox("No Selecciono el Archivo", MsgBoxStyle.Information)
        '    Else
        '        DameClv_Session_Oxxo()
        '        LocNomarchivo = Me.OpenFileDialog1.FileName
        '        Dim POSICION_1 As Integer = 0
        '        Dim Consecutivo As String = Nothing
        '        Dim POSICION_2 As Integer = 0
        '        Dim Tienda As String = Nothing
        '        Dim POSICION_3 As Integer = 0
        '        Dim Fecha As String = Nothing
        '        Dim POSICION_4 As Integer = 0
        '        Dim Hora As String = Nothing
        '        Dim POSICION_5 As Integer = 0
        '        Dim RECIBO As String = Nothing
        '        Dim POSICION_6 As Integer = 0
        '        Dim TEMP As String = Nothing
        '        Dim POSICION_7 As Integer = 0
        '        Dim MONTO As String = Nothing

        '        Dim archivo2 As TextReader = New StreamReader(Me.OpenFileDialog1.FileName)
        '        'Dim archivo3 As TextReader = New StreamReader(Me.OpenFileDialog1.FileName)
        '        While archivo2.Peek() <> -1
        '            Fila_1 = archivo2.ReadLine()
        '            POSICION_1 = Fila_1.IndexOfAny(",", 0)
        '            Consecutivo = Mid(Fila_1, 1, POSICION_1)
        '            POSICION_2 = Fila_1.IndexOfAny(",", POSICION_1 + 1)
        '            Tienda = Mid(Fila_1, POSICION_1 + 2, POSICION_2 - (POSICION_1 + 1))
        '            POSICION_3 = Fila_1.IndexOfAny(",", POSICION_2 + 1)
        '            Fecha = Mid(Fila_1, POSICION_2 + 2, POSICION_3 - (POSICION_2 + 1))
        '            POSICION_4 = Fila_1.IndexOfAny(",", POSICION_3 + 1)
        '            Hora = Mid(Fila_1, POSICION_3 + 2, POSICION_4 - (POSICION_3 + 1))
        '            POSICION_5 = Fila_1.IndexOfAny(",", POSICION_4 + 1)
        '            RECIBO = Mid(Fila_1, POSICION_4 + 7, POSICION_5 - (POSICION_4 + 19))
        '            POSICION_6 = Fila_1.IndexOfAny(",", POSICION_5 + 1)
        '            TEMP = Mid(Fila_1, POSICION_5 + 2, POSICION_6 - (POSICION_5 + 1))
        '            POSICION_7 = Len(Fila_1)
        '            MONTO = Mid(Fila_1, POSICION_6 + 2, POSICION_7 - (POSICION_6 + 1))
        '            ''
        '            CON.Open()
        '            Dim comando As SqlClient.SqlCommand
        '            comando = New SqlClient.SqlCommand
        '            With comando
        '                .Connection = CON
        '                .CommandText = "EXEC GUARDA_Resultado_Oxxo " & Clv_Session_Oxxo & ",'" & Consecutivo & "','" & Tienda & "','" & Fecha & "','" & Hora & "','" & RECIBO & "','" & TEMP & "','" & MONTO & "'"
        '                .CommandType = CommandType.Text
        '                .CommandTimeout = 0
        '                .ExecuteReader()
        '            End With
        '            CON.Close()
        '            y += 1
        '        End While
        '        archivo2.Close()


        '        'archivo3.Close()
        '    End If
        '    CON.Open()
        '    Me.CONSULTA_Resultado_OxxoTableAdapter.Connection = CON
        '    Me.CONSULTA_Resultado_OxxoTableAdapter.Fill(Me.DataSetEdgar.CONSULTA_Resultado_Oxxo, Clv_Session_Oxxo)
        '    CON.Close()
        '    bitsist(GloUsuario, 0, GloSistema, Me.Name, "Se Abrio Un Archivo De Proceso de Oxxo", "", "Nombre Del Archivo:" + LocNomarchivo, SubCiudad)

        '    LlenaCONSULTATablaClv_Session_Oxxo()

        '    '========Afecto Clientes que Pasaron el el Archivo========00
        '    'If IsNumeric(Me.Clv_SessionBancosTextBox.Text) = True Then
        '    ' proceso = CLng(Me.Clv_SessionBancosTextBox.Text)
        '    'Me.Procesa_Arhivo_santaderTableAdapter.Connection = CON
        '    'Me.Procesa_Arhivo_santaderTableAdapter.Fill(Me.Procedimientos_arnoldo.Procesa_Arhivo_santader, proceso, locerrorsantander)
        '    'CON.Close()
        '    'If locerrorsantander = 0 Then
        '    ' MsgBox("Archivo Procesado Exitosamente", MsgBoxStyle.Information)
        '    ' DameDetalle()
        '    ' ElseIf locerrorsantander = 1 Then
        '    ' MsgBox("El Número De Proceso No Corresponde Al Archivo Seleccionado", MsgBoxStyle.Information)
        '    ' End If
        '    'End If
        'Catch ex As Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try

        'SAUL
        uspAutorizadoRecepciónOxxo()
        If bndOxxo = 0 Then
            locband_pant = 9
            Dim Supervisor As New FrmSupervisor
            GloNotasC = True
            Supervisor.Text = "Seguridad Recepción del Archivo de Oxxo"
            Supervisor.ShowDialog()
            GloNotasC = False
            If Supervisor.DialogResult = Windows.Forms.DialogResult.OK Then
                locband_pant = 0
            Else
                locband_pant = 0
                Exit Sub
            End If
        End If
        bndOxxo = 0
        '(Fin)
        Try

            Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            Dim prueba As String = Nothing
            Dim locerrorsantander As Integer = 0
            Dim proceso As Integer = 0
            Dim x As Integer = 0
            Dim y As Integer = 0
            Dim z As Integer = 0
            Dim clv_aceptado As String = Nothing
            Dim clv_id As String = Nothing
            Dim contrato As String = Nothing
            Dim Fila_1 As String = Nothing
            Dim DT As New DataTable


            Me.OpenFileDialog1.FileName = ""
            'Me.OpenFileDialog1.Filter = "Archivo Resultados *.txt|*.txt"
            Me.OpenFileDialog1.ShowDialog()


            If Me.OpenFileDialog1.FileName = "" Or Me.OpenFileDialog1.FileName = "OpenFileDialog1" Then
                MsgBox("No Selecciono el Archivo", MsgBoxStyle.Information)
            Else
                LocNomarchivo = Me.OpenFileDialog1.SafeFileName
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@ARCHIVO", SqlDbType.VarChar, LocNomarchivo, 500)
                DT = BaseII.ConsultaDT("UspValidaArchivoOXXO")
                If DT.Rows(0)(0).ToString = "1" Then
                    MsgBox("El Archivo ya está cargado, no se puede cargar de nuevo", MsgBoxStyle.Information)
                    Exit Sub
                End If

                Dim bandera As Boolean = False
                For Each dgvr As DataGridViewRow In dgvCargaArchivos.Rows
                    If dgvr.Cells(2).Value.ToString = LocNomarchivo Then
                        bandera = True
                    End If
                Next
                If bandera = False Then
                    DameClv_Session_Oxxo(LocNomarchivo)
                    Dim Consecutivo As String = Nothing
                    Dim Tienda As String = Nothing
                    Dim Fecha As String = Nothing
                    Dim Hora As String = Nothing
                    Dim cliente As String = Nothing
                    Dim TEMP As String = Nothing
                    Dim MONTO As String = Nothing
                    Dim archivo2 As TextReader = New StreamReader(Me.OpenFileDialog1.FileName)
                    While archivo2.Peek() <> -1
                        Fila_1 = archivo2.ReadLine()
                        Consecutivo = Fila_1.Substring(0, 2)
                        Tienda = Fila_1.Substring(3, 25)
                        Fecha = Fila_1.Substring(29, 8)
                        Hora = Fila_1.Substring(38, 5)
                        cliente = Fila_1.Substring(46, 5)
                        TEMP = Fila_1.Substring(67, 25)
                        MONTO = Fila_1.Substring(93, 16)
                        If Tienda <> "REGISTRO DE CONTROL      " Then
                            BaseII.limpiaParametros()
                            BaseII.CreateMyParameter("@Clv_Session", System.Data.SqlDbType.BigInt, Clv_Session_Oxxo)
                            BaseII.CreateMyParameter("@Consecutivo", System.Data.SqlDbType.BigInt, CInt(Consecutivo))
                            BaseII.CreateMyParameter("@Tienda", System.Data.SqlDbType.VarChar, Tienda, 50)
                            BaseII.CreateMyParameter("@Fecha", System.Data.SqlDbType.VarChar, Fecha, 8)
                            BaseII.CreateMyParameter("@Hora", System.Data.SqlDbType.VarChar, Hora, 5)
                            BaseII.CreateMyParameter("@Cliente", System.Data.SqlDbType.BigInt, CInt(cliente))
                            BaseII.CreateMyParameter("@TEMP", System.Data.SqlDbType.VarChar, TEMP, 50)
                            BaseII.CreateMyParameter("@MONTO", System.Data.SqlDbType.Decimal, CDbl(MONTO))
                            BaseII.Inserta("Guarda_Resultado_Oxxo")
                        End If
                        y += 1
                    End While
                    archivo2.Close()

                    LlenaCONSULTATablaClv_Session_Oxxo()
                    LlenadgvDetalleArchivoCargado(Clv_Session_Oxxo)

                Else
                    MsgBox("El Archivo ya fue cargado", MsgBoxStyle.Information)
                End If

            End If

        Catch ex As Exception

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_Sesion", SqlDbType.BigInt, Clv_Session_Oxxo)
            BaseII.CreateMyParameter("@Estado", SqlDbType.VarChar, "F", 1)
            BaseII.Inserta("uspModificaTablaClv_Session_Oxxo")
            System.Windows.Forms.MessageBox.Show("El archivo esta en un formato incorrecto")
            LlenaCONSULTATablaClv_Session_Oxxo()
            LlenadgvDetalleArchivoCargado(Clv_Session_Oxxo)

        End Try

    End Sub


    Private Sub FrmRecepOxxo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        LlenaCONSULTATablaClv_Session_Oxxo()
        If dgvCargaArchivos.Rows.Count = 0 Then
            LlenadgvDetalleArchivoCargado(0)
        Else
            LlenadgvDetalleArchivoCargado(CInt(dgvCargaArchivos.SelectedRows(0).Cells(0).Value.ToString))
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub




    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub CONSULTA_Resultado_OxxoDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        'If e.ColumnIndex = 9 Then
        '    If Me.CONSULTA_Resultado_OxxoDataGridView.RowCount > 0 Then
        '        If IsNumeric(Me.ReciboTextBox.Text) = True Then
        '            GloClv_Recibo = Me.ReciboTextBox.Text
        '            FrmVisorOxxo.Show()
        '        Else
        '            MsgBox("El No. Recibo no es valido ", MsgBoxStyle.Information)
        '        End If
        '    End If
        'End If
    End Sub

    Private Sub CONSULTA_Resultado_OxxoDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

    End Sub

    Private Sub CONSULTA_Resultado_OxxoDataGridView_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

    End Sub

    Private Sub CLIENTELabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub CLIENTETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CLIENTETextBox.TextChanged
        If IsNumeric(Me.CLIENTETextBox.Text) = True Then
            GloContrato = Me.CLIENTETextBox.Text
            Glocontratosel = Me.CLIENTETextBox.Text
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim identi As Long = 0
        Dim DT As New DataTable
        Dim I As Integer
        'Try
        '    Dim BndError As Integer = 0
        '    Dim Msg As String = Nothing
        '    Dim CON As New SqlConnection(MiConexion)
        '    Dim I As Integer = 0
        '    Dim x As Long = 0
        '    If Mid(Me.StatusTextBox.Text, 1, 1) = "A" Then
        '        If Me.CONSULTA_Resultado_OxxoDataGridView.RowCount > 0 Then
        '            For I = 0 To Me.CONSULTA_Resultado_OxxoDataGridView.RowCount - 1
        '                If IsNumeric((Me.CONSULTA_Resultado_OxxoDataGridView.Item(0, I).Value)) = True Then
        '                    CON.Open()
        '                    Dim comando As SqlClient.SqlCommand
        '                    comando = New SqlClient.SqlCommand
        '                    With comando
        '                        .Connection = CON
        '                        .CommandText = "GuardaPagoOXXO "
        '                        .CommandType = CommandType.StoredProcedure
        '                        .CommandTimeout = 0
        '                        Dim prm As New SqlParameter("@Clv_Recibo", SqlDbType.BigInt)
        '                        prm.Direction = ParameterDirection.Input
        '                        prm.Value = Me.CONSULTA_Resultado_OxxoDataGridView.Item(4, I).Value
        '                        .Parameters.Add(prm)

        '                        Dim prm1 As New SqlParameter("@BndError_1", SqlDbType.Int)
        '                        prm1.Direction = ParameterDirection.Output
        '                        prm1.Value = 0
        '                        .Parameters.Add(prm1)

        '                        Dim prm2 As New SqlParameter("@Msg_1", SqlDbType.VarChar)
        '                        prm2.Direction = ParameterDirection.Output
        '                        prm2.Value = ""
        '                        .Parameters.Add(prm2)

        '                        Dim j As Integer = comando.ExecuteNonQuery()
        '                        BndError = prm1.Value
        '                        Msg = prm2.Value
        '                        If BndError > 0 Then
        '                            MsgBox(Msg)
        '                        End If

        '                    End With
        '                    CON.Close()
        '                End If
        '            Next
        '            CON.Open()
        '            Dim comando1 As SqlClient.SqlCommand
        '            comando1 = New SqlClient.SqlCommand
        '            With comando1
        '                .Connection = CON
        '                .CommandText = "Update TablaClv_Session_Oxxo Set Status='P' Where  Clv_Session= " & Clv_Session_Oxxo
        '                .CommandType = CommandType.Text
        '                .CommandTimeout = 0
        '                .ExecuteReader()
        '            End With
        '            CON.Close()
        '            LlenaCONSULTATablaClv_Session_Oxxo()
        '            bitsist(GloUsuario, 0, GloSistema, Me.Name, "Se Afectaron los Clientes del Archivo Oxxo", "", "Nombre del Archivo:" + LocNomarchivo, SubCiudad)
        '            MsgBox("Se han procesado con éxito todos los cobros ", MsgBoxStyle.Information)
        '        Else
        '            MsgBox("No ahi datos para procesarlos ", MsgBoxStyle.Information)
        '        End If
        '    ElseIf Mid(Me.StatusTextBox.Text, 1, 1) = "C" Then
        '        MsgBox("No se puede afectar por que esta cancelado este proceso ", MsgBoxStyle.Information)
        '    Else
        '        MsgBox("No se puede afectar por que ya fue afectado anteriormente ", MsgBoxStyle.Information)
        '    End If
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        ''''End Try

        'SAUL
        'uspAutorizadoRecepciónOxxo()
        'If bndOxxo = 0 Then
        '    locband_pant = 9
        '    Dim Supervisor As New FrmSupervisor
        '    GloNotasC = True
        '    Supervisor.Text = "Seguridad Recepción del Archivo de Oxxo"
        '    Supervisor.ShowDialog()
        '    GloNotasC = False
        '    If Supervisor.DialogResult = Windows.Forms.DialogResult.OK Then
        '        locband_pant = 0
        '    Else
        '        locband_pant = 0
        '        Exit Sub
        '    End If
        'End If
        'bndOxxo = 0
        '(Fin) 

        If dgvCargaArchivos.Rows.Count <> 0 Then
            If dgvCargaArchivos.SelectedCells(4).Value.ToString = "Pendiente" Then
                Dim eClv_Factura As Long = 0
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLVSESSION", System.Data.SqlDbType.BigInt, CInt(dgvCargaArchivos.SelectedRows(0).Cells(0).Value))
                'BaseII.CreateMyParameter("@Cajera", System.Data.SqlDbType.VarChar, GloUsuario)
                'BaseII.CreateMyParameter("@Sucursal", System.Data.SqlDbType.Int, GloSucursal)
                'BaseII.CreateMyParameter("@Caja", System.Data.SqlDbType.Int, GloCaja)
                DT = BaseII.ConsultaDT("uspAfectaClientesOxxoNew")

                'For I = 0 To DT.Rows.Count - 1
                '    GloClv_Factura = CInt(DT.Rows(I)(0).ToString)

                '    identi = 0
                '    identi = BusFacFiscalOledb(GloClv_Factura)
                '    'fin Facturacion Digital
                '    locID_Compania_Mizart = ""
                '    locID_Sucursal_Mizart = ""
                '    'Fin Guarda si el tipo de Pago es con Nota de Credito
                '    ''FIN FACTURACION DIGITAL EDGAR 4/FEB/2012

                '    '   FIN FACTURACION DIGITAL EDGAR 4/FEB/2012

                '    If CInt(identi) > 0 Then
                '        ''SAUL IMPRIMIR FACTURA FISCAL
                '        ''LiTipo = 2
                '        ''GloOpFacturas = 1
                '        ''FrmImprimir.ShowDialog()
                '        ''SAUL IMPRIMIR FACTURA FISCAL(FIN)

                '        ' ''Inicio Facturacion Digital
                '        'Locop = 0
                '        'Dime_Aque_Compania_Facturarle(GloClv_Factura)
                '        'Graba_Factura_Digital(GloClv_Factura, identi)
                '        ' ''FrmImprimirFacturaDigital.Show()
                '        ''FormPruebaDigital.Show()
                '        'locID_Compania_Mizart = ""
                '        'locID_Sucursal_Mizart = ""
                '        ' ''fin Facturacion Digital
                '        Locop = 0
                '        Dime_Aque_Compania_Facturarle(GloClv_Factura)
                '        Graba_Factura_Digital(GloClv_Factura, identi)
                '        'FrmImprimirFacturaDigital.Show()
                '        'Try
                '        '    FormPruebaDigital.Show()
                '        'Catch ex As Exception

                '        'End Try

                '        locID_Compania_Mizart = ""
                '        locID_Sucursal_Mizart = ""

                '    End If
                'Next

                'Dim CONE As New SqlConnection(MiConexion)
                'Dim comando As SqlClient.SqlCommand
                'Dim reader As SqlDataReader
                'Dim exClv_Factura As Long = 0
                'CONE.Open()
                'comando = New SqlClient.SqlCommand
                'With comando
                '    .Connection = CONE
                '    .CommandText = "EXEC USPGENERA_FACDIG_OXXO " & CInt(dgvCargaArchivos.SelectedRows(0).Cells(0).Value)
                '    .CommandType = CommandType.Text
                '    .CommandTimeout = 0
                '    reader = comando.ExecuteReader()
                '    Using reader
                '        While reader.Read
                '            exClv_Factura = CLng(reader.GetValue(0))
                '            If exClv_Factura > 0 Then
                '                Genera_Factura_Digital(exClv_Factura)
                '                GeneraDocumentoTxt_DIGITAL(exClv_Factura, "\\192.168.123.11\exes\softv\FacturaDig")
                '            End If
                '        End While
                '    End Using
                'End With
                'CONE.Close()
                LlenaCONSULTATablaClv_Session_Oxxo()
                LlenadgvDetalleArchivoCargado(CInt(dgvCargaArchivos.SelectedRows(0).Cells(0).Value.ToString))
            ElseIf dgvCargaArchivos.SelectedCells(4).Value.ToString = "Cancelada" Then
                MsgBox("El Archivo esta cancelado", MsgBoxStyle.Information)
            Else
                MsgBox("El Archivo ya fue afectado", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("No hay Archivos para Afectar", MsgBoxStyle.Information)
        End If


    End Sub
    Private Sub LlenadgvDetalleArchivoCargado(ByVal clvsesion As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", System.Data.SqlDbType.BigInt, clvsesion)
        Dim DT As New DataTable
        DT = BaseII.ConsultaDT("CONSULTA_Resultado_Oxxo")
        dgvDetalleArchivoCargado.DataSource = DT
        'dgvDetalleArchivoCargado.Columns(0).Visible = False
        'dgvDetalleArchivoCargado.Columns(8).Visible = False
    End Sub
    Private Sub LlenaCONSULTATablaClv_Session_Oxxo()
        'Try
        '    Dim Con As New SqlConnection(MiConexion)
        '    Con.Open()
        '    Me.CONSULTATablaClv_Session_OxxoTableAdapter.Connection = Con
        '    Me.CONSULTATablaClv_Session_OxxoTableAdapter.Fill(Me.DataSetEdgar.CONSULTATablaClv_Session_Oxxo, 0)
        '    Con.Close()
        'Catch ex As System.Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try
        BaseII.limpiaParametros()
        Dim DT As New DataTable
        DT = BaseII.ConsultaDT("CONSULTATablaClv_Session_Oxxo")
        dgvCargaArchivos.DataSource = DT

    End Sub

    Private Sub Clv_SessionLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Clv_SessionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_SessionTextBox.TextChanged
        If IsNumeric(Clv_SessionTextBox.Text) = True Then
            Dim Con As New SqlConnection(MiConexion)
            Con.Open()
            Clv_Session_Oxxo = Clv_SessionTextBox.Text
            Me.CONSULTA_Resultado_OxxoTableAdapter.Connection = Con
            Me.CONSULTA_Resultado_OxxoTableAdapter.Fill(Me.DataSetEdgar.CONSULTA_Resultado_Oxxo, Clv_Session_Oxxo)
            Con.Close()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim Con As New SqlConnection(MiConexion)
        If Mid(Me.StatusTextBox.Text, 1, 1) = "A" Then
            Con.Open()
            Dim comando1 As SqlClient.SqlCommand
            comando1 = New SqlClient.SqlCommand
            With comando1
                .Connection = Con
                .CommandText = "Update TablaClv_Session_Oxxo Set Status='C' Where  Clv_Session= " & Clv_Session_Oxxo
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                .ExecuteReader()
            End With
            Con.Close()
            LlenaCONSULTATablaClv_Session_Oxxo()
            bitsist(GloUsuario, 0, GloSistema, Me.Name, "Se Cancelo el Proceso del Oxxo", "", "Nombre del Archivo:" + LocNomarchivo, SubCiudad)
        ElseIf Mid(Me.StatusTextBox.Text, 1, 1) = "A" Then
            MsgBox("Ya esta cancelado este proceso ", MsgBoxStyle.Information)
        Else
            MsgBox("Solo se pueden cancelar procesos con status Activo", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Try
            If Mid(Me.StatusTextBox.Text, 1, 1) = "P" Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Dim comando As SqlClient.SqlCommand
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "DAMEFACTURASOXXO"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = Clv_SessionTextBox.Text
                    .Parameters.Add(prm)

                    Dim prm1 As New SqlParameter("@INI", SqlDbType.BigInt)
                    prm1.Direction = ParameterDirection.Output
                    prm1.Value = 0
                    .Parameters.Add(prm1)

                    Dim prm2 As New SqlParameter("@FIN", SqlDbType.BigInt)
                    prm2.Direction = ParameterDirection.Output
                    prm2.Value = 0
                    .Parameters.Add(prm2)
                    Dim i As Integer = comando.ExecuteNonQuery()
                    ClvFacturaIni = prm1.Value
                    ClvFacturaFin = prm2.Value
                    CON.Close()
                End With

                ConfigureCrystalReports(ClvFacturaIni, ClvFacturaFin)

            Else
                MsgBox("Solo se pueden imprimir las facturas de los proceso que tiene Status de Procesado ", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CONSULTATablaClv_Session_OxxoDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If IsNumeric(Me.Clv_SessionTextBox.Text) = True Then
            GloClv_SessionBancos = Me.Clv_SessionTextBox.Text
            GloReporte = 9
            My.Forms.FrmImprimirRepGral.Show()
        End If
    End Sub

    Private Sub dgvCargaArchivos_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvCargaArchivos.CurrentCellChanged
        Try
            LlenadgvDetalleArchivoCargado(CInt(dgvCargaArchivos.SelectedRows(0).Cells(0).Value.ToString))
        Catch ex As Exception
        End Try
    End Sub
    Private Sub uspAutorizadoRecepciónOxxo()
        'GloUsuario-bndOxxo
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, GloUsuario, 10)
        BaseII.CreateMyParameter("@bnd", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("uspAutorizadoRecepciónOxxo")
        bndOxxo = CInt(BaseII.dicoPar("@bnd").ToString)
    End Sub

    Private Sub Button4_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim DT As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, CInt(dgvCargaArchivos.SelectedRows(0).Cells(0).Value.ToString))
        DT = BaseII.ConsultaDT("UspCancelacionArchivosOXXO")
        If DT.Rows(0)(0).ToString = "1" Then
            MsgBox("El Archivo no puede ser cancelado", MsgBoxStyle.Information)
        ElseIf DT.Rows(0)(0).ToString = "2" Then
            MsgBox("El Archivo ya estaba cancelado", MsgBoxStyle.Information)
        Else
            MsgBox("El Archivo fue cancelado con éxito", MsgBoxStyle.Information)

        End If
        LlenaCONSULTATablaClv_Session_Oxxo()
        LlenadgvDetalleArchivoCargado(CInt(dgvCargaArchivos.SelectedRows(0).Cells(0).Value.ToString))
    End Sub
End Class